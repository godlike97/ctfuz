from telepot.namedtuple import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from bot.management.commands import constants

user_markup = ReplyKeyboardMarkup(keyboard=[
    [constants.USER_MENU[0], constants.USER_MENU[1]],
    [constants.USER_MENU[2]],
], resize_keyboard=True)

back_markup = ReplyKeyboardMarkup(keyboard=[
    [constants.GO_BACK[0]]
], resize_keyboard=True)

number_markup = ReplyKeyboardMarkup(keyboard=[
    ['2', '3', ''],
    ['4', '5', '6'],
    ['7', '8', '9'],
    [' ', '0', ' ']
], resize_keyboard=True)

skip_markup = ReplyKeyboardMarkup(keyboard=[
    [constants.SKIP[0]],
    [constants.GO_BACK[0]],
],resize_keyboard=True)