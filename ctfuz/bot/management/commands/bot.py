import ast
import json
from telepot.namedtuple import InlineKeyboardMarkup
import time
import telepot
from telepot.loop import MessageLoop
from bot.management.commands import constants
from bot.management.commands.actions import create_task
from bot.management.commands.functions import start, tasks
from bot.management.commands.markups import *
from web.models import UserTemp, UserInfo


def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    text = ''
    if content_type == 'text':
        text = msg['text']
    try:
        usr = UserTemp.objects.filter(t_id=chat_id).first()
    except UserTemp.DoesNotExist:
        usr = UserTemp()
        usr.t_id = chat_id
        usr.state = '0'
        usr.save()
    if usr is None:
        usr = UserTemp()
        usr.t_id = chat_id
        usr.state = '0'
        usr.save()

    if content_type == 'text':
        text = msg['text']
        user = msg['from']['first_name']
        print(user, text, usr.state)
        if text == '/start' or usr.state in constants.REG:
            if text == '/start':
                usr.state = '0'
                usr.state_text = '[]'
                usr.save()
            start.start(bot, msg)
        if text == constants.USER_MENU[0] or usr.state in constants.TASKS:
            tasks.handle(bot, msg)
        if text == constants.USER_MENU[2] or usr.state in constants.ADD_TASK:
            tasks.handle(bot, msg)
        if text == constants.USER_MENU[1]:
            _users = UserInfo.objects.order_by('-total_points')
            _text = ''
            i = 1
            for _usr in _users:
                _text += str(i) + '.\t' + _usr.username + '\t' + str(_usr.total_points) + '\n'
                i += 1
            bot.sendMessage(chat_id, _text, reply_markup=user_markup)
    if usr.state == 'a6':
        if text != '':
            if text != constants.GO_BACK[0]:
                bot.sendMessage(chat_id, "Я не думаю что это файл", reply_markup=back_markup)
            elif text == constants.GO_BACK[0]:
                usr.state = 'a5'
                usr.save()
                bot.sendMessage(chat_id, 'Введите ответ задания', reply_markup=back_markup)
        if content_type in ['document', 'photo', 'audio']:
            if content_type == 'document':
                file_id = msg['document']['file_id']
            elif content_type == 'photo':
                photos = msg['photo']
                file_id = photos[len(photos) - 1]['file_id']
            else:
                file_id = msg['audio']['file_id']
            state_json = ast.literal_eval(usr.state_text)
            for state in state_json:
                state['file'] = file_id
            usr.state_text = json.dumps(state_json)
            usr.save()
            chk = create_task(usr)
            if chk:
                bot.sendMessage(chat_id, 'Ура! Еще одно задание в копилке. Благодарности выше крыши',
                                    reply_markup=user_markup)
            else:
                bot.sendMessage(chat_id, 'Увы! Что-то не-то в процессе добавления задания. '
                                             'Пожалуйста попробуй сообщить об этом @B1GBO5S у. '
                                             'Он точно скажет почему не получилось',
                                    reply_markup=user_markup)
            usr.state = '0'
            usr.state_text = '[]'
            usr.save()


def on_callback_query(msg):
    query_id, from_id, data = telepot.glance(msg, flavor='callback_query')
    chat_id = msg['message']['chat']['id']
    from_id = msg['from']['id']
    first_name = msg['from']['first_name']
    try:
        usr = UserTemp.objects.filter(t_id=chat_id).first()
    except UserTemp.DoesNotExist:
        usr = UserTemp()
        usr.t_id = chat_id
        usr.state = '0'
        usr.save()
    if 'ans' in data:
        task_id = data.split('ans_')[1]
        usr.state = 'm2'
        usr.state_text = '[]'
        state_json = ast.literal_eval(usr.state_text)
        _user = {
            'task': task_id
        }
        state_json.append(_user)
        usr.state_text = json.dumps(state_json)
        usr.save()

        bot.answerCallbackQuery(query_id, "Отправьте ответ")


bot = telepot.Bot(constants.TOKEN)
MessageLoop(bot, {
    'chat': handle,
    'callback_query': on_callback_query,
}).run_as_thread()
print('listening ...')

while 1:
    time.sleep(10)
