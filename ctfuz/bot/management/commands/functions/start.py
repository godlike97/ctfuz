import ast
import json
import re

from django.contrib.auth import authenticate

from bot.management.commands.actions import create_user, contact_validate
from bot.management.commands.markups import *
from bot.management.commands import constants
import telepot

from web.models import UserInfo, UserTemp


def start(bot, msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    from_id = msg['from']['id']
    text = msg['text']
    bot.sendChatAction(chat_id, 'typing')
    try:
        UserInfo.objects.get(t_id=from_id)
        bot.sendMessage(chat_id, "С возвращением сэр", reply_markup=user_markup)
    except UserInfo.DoesNotExist:
        try:
            usr = UserTemp.objects.get(t_id=chat_id)
            if usr.state == '0':
                usr.state = 'r1'
                usr.save()
        except UserTemp.DoesNotExist:
            usr = UserTemp()
            usr.t_id = chat_id
            usr.state = 'r1'
            usr.save()
        if usr.state in constants.REG:
            if usr.state == 'r1':
                if (text != constants.GO_BACK[0]) and (text not in constants.SKIP):
                    usr.state = 'r2'
                    usr.state_text = '[]'
                    state_json = ast.literal_eval(usr.state_text)
                    _user = {
                        'tid': from_id,
                        'username': '',
                        'phone': '',
                        'email': '',
                        'city': ''
                    }
                    state_json.append(_user)
                    usr.state_text = json.dumps(state_json)
                    usr.save()
                    bot.sendMessage(chat_id, 'Введите свой ник(username)',
                                    reply_markup=back_markup)
                if text in constants.GO_BACK:
                    usr.state = 'r2'
                    bot.sendMessage(chat_id, 'Введите свой ник(username)',
                                    reply_markup=back_markup)
            elif usr.state == 'r2':
                if text != constants.GO_BACK[0]:
                    state_json = ast.literal_eval(usr.state_text)
                    for state in state_json:
                        state['username'] = text
                    usr.state_text = json.dumps(state_json)
                    usr.state = 'r3'
                    usr.save()
                    bot.sendMessage(chat_id, 'Введите свой email', reply_markup=back_markup)
                elif text == constants.GO_BACK[0]:
                    usr.state = 'r2'
                    usr.save()
                    bot.sendMessage(chat_id, 'Введите свой ник(username)',
                                    reply_markup=back_markup)

            elif usr.state == 'r3':
                if text != constants.GO_BACK[0]:
                    if re.match(r"[^@]+@[^@]+\.[^@]+", text):
                        state_json = ast.literal_eval(usr.state_text)
                        for state in state_json:
                            state['email'] = text
                        usr.state_text = json.dumps(state_json)
                        usr.state = 'r4'
                        usr.save()
                        bot.sendMessage(chat_id, 'Введите свой номер телефона в формате (+9989xxxxxxxx или 9xxxxxxxx)',
                                        reply_markup=back_markup)
                    else:
                        bot.sendMessage(chat_id, 'Давайте будем людьми и напишем правильный email',
                                        reply_markup=back_markup)
                elif text == constants.GO_BACK[0]:
                    usr.state = 'r2'
                    bot.sendMessage(chat_id, 'Введите свой ник(username)', reply_markup=back_markup)
                    usr.save()
            elif usr.state == 'r4':
                if text != constants.GO_BACK[0]:
                    check_text = text.replace(' ', '').replace('-', '').replace('+', '')
                    if contact_validate(check_text):
                        state_json = ast.literal_eval(usr.state_text)
                        for state in state_json:
                            state['phone'] = text.replace(' ', '').replace(';', '; ').replace(',', ', ')
                        usr.state_text = json.dumps(state_json)
                        usr.state = 'r5'
                        usr.save()
                        bot.sendMessage(chat_id, 'Введите ваш город', reply_markup=back_markup)
                    else:
                        bot.sendMessage(chat_id, 'Опять дватцать пять. Так трудно написать свой номер',
                                        reply_markup=back_markup)
                if text == constants.GO_BACK[0]:
                    usr.state = 'r3'
                    usr.save()
                    bot.sendMessage(chat_id, 'Введите свой email',
                                    reply_markup=back_markup)
            elif usr.state == 'r5':
                if text != constants.GO_BACK[0]:
                    state_json = ast.literal_eval(usr.state_text)
                    for state in state_json:
                        state['city'] = text
                    usr.state_text = json.dumps(state_json)
                    usr.state = 'r6'
                    usr.save()
                elif text == constants.GO_BACK[0]:
                    usr.state = 'r4'
                    usr.save()
                    bot.sendMessage(chat_id, 'Введите свой номер телефона в формате (+9989xxxxxxxx или 9xxxxxxxx)')
            if usr.state == 'r6':
                if text != constants.GO_BACK[0]:
                    chk = create_user(usr)
                    if chk:
                        usr.state = '0'
                        usr.state_text = '[]'
                        usr.save()
                        bot.sendMessage(chat_id,
                                        'Теперь смело пользуйтесь ботом. Удачи в захвате флагов',
                                        reply_markup=user_markup)
