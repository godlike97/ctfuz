import ast
import json
import re

from django.contrib.auth import authenticate

from bot.management.commands.actions import create_user, contact_validate
from bot.management.commands.markups import *
from bot.management.commands import constants
from web.models import *
import telepot


def handle(bot, msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    from_id = msg['from']['id']
    text = msg['text']
    bot.sendChatAction(chat_id, 'typing')

    try:
        usr = UserTemp.objects.get(t_id=chat_id)
    except UserTemp.DoesNotExist:
        usr = UserTemp()
        usr.t_id = chat_id
        usr.state = '0'
        usr.save()

    if usr.state in constants.TASKS:
        if usr.state == 'm2':
            try:
                state_json = ast.literal_eval(usr.state_text)
                task_id = 1
                for state in state_json:
                    task_id = state['task']
                task = Task.objects.get(pk=int(task_id))
                true_answer = task.answer
                user_answer = text
                _categories = Category.objects.all()
                _cat_markup = []
                for cat in _categories:
                    _cat_markup.append([cat.name])
                _cat_markup.append([constants.GO_BACK[0]])
                usr.state = 'm1'
                usr.save()
                if true_answer == user_answer:

                    _uzver = UserInfo.objects.get(t_id=usr.t_id)
                    _solved_tasks = _uzver.solved_tasks.replace('[', '').replace(']', '').split(',')
                    _solved_tasks = list(map(int, _solved_tasks))
                    if task.pk not in _solved_tasks:
                        if _uzver.solved_tasks == '[]':
                            _uzver.solved_tasks = str(task_id)
                        else:
                            _uzver.solved_tasks = _uzver.solved_tasks + ',' + str(task_id)
                        _uzver.total_points += task.point
                        if task.first != _uzver or task.second != _uzver or task.third != _uzver:
                            if task.first is None:
                                task.first = _uzver
                            elif task.second is None:
                                task.second = _uzver
                            elif task.third is None:
                                task.third = _uzver
                            task.save()
                        _uzver.save()
                        usr.state = 'm1'
                        usr.state_text = '[]'
                        usr.save()
                        bot.sendMessage(chat_id, "Молодец! Ты решил задание. Я думаю тебе понравилось " +
                                        _uzver.username,
                                        reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup, resize_keyboard=True))
                    elif task.pk in _solved_tasks:
                        bot.sendMessage(chat_id, "Хотель попробовать повысить рейтинг этим путём 😁. Но увы ты опоздаль"
                                                 "Этот баг уже убран 😂😂😂.")
                else:
                    bot.sendMessage(chat_id, "Увы! Но не расстраивайтесь. Тем не менее не здавайтесь!",
                                    reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup, resize_keyboard=True))
            except:
                usr.state = 'm1'
                usr.state_text = '[]'
                usr.save()
                bot.sendMessage(chat_id, "Это задание не найдено или удалено")
        elif usr.state == 'm1':
            if text != constants.GO_BACK[0]:
                try:
                    _uzver = UserInfo.objects.get(t_id=usr.t_id)
                    _solved_tasks = _uzver.solved_tasks.replace('[', '').replace(']', '').split(',')
                    _solved_tasks = list(map(int, _solved_tasks))
                    _category = Category.objects.get(name=text)
                    _tasks = Task.objects.filter(category=_category)
                    for task in _tasks.filter(point__gte=0):
                        _first = 'Здесь можешь быть ты'
                        _second = 'Здесь можешь быть ты'
                        _third = 'Здесь можешь быть ты'
                        if task.first is not None:
                            _first = task.first.username
                        if task.second is not None:
                            _second = task.second.username
                        if task.third is not None:
                            _third = task.third.username
                        _task_text = str(task.name) + '\n' + str(task.text) + '\n\n' + 'Баллов: ' + str(task.point) + \
                                     '\n\n' + '🥇' + _first + '\n' + '🥈' + _second + '\n' + '🥉' + _third
                        if task.pk in _solved_tasks:
                            _reply_markup = None
                        else:
                            _reply_markup =InlineKeyboardMarkup(inline_keyboard=[
                               [dict(text="Ответить", callback_data="ans_" + str(task.pk))]
                            ])
                        if task.file is None or task.file == '':
                            bot.sendMessage(chat_id, _task_text, reply_markup= _reply_markup)
                        else:
                            bot.sendDocument(chat_id,document=task.file, caption=_task_text, reply_markup=_reply_markup)
                except Exception as ex:
                    print(ex)
                    _categories = Category.objects.all()
                    _cat_markup = []
                    for cat in _categories:
                        _cat_markup.append([cat.name])
                    _cat_markup.append([constants.GO_BACK[0]])
                    bot.sendMessage(chat_id, 'Еще нет заданий в этой категории. Но не огорчайтесь, '
                                             'вы можете быть первым '
                                             'кто добавит задание. Этим самым вы поможете проекту.',
                                    reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup, resize_keyboard=True))

            elif text == constants.GO_BACK[0]:
                usr.state = 0
                usr.state_text = "[]"
                usr.save()
                bot.sendMessage(chat_id, "Выберите раздел", reply_markup=user_markup)
    if text == constants.USER_MENU[0]:
        _categories = Category.objects.all()
        _cat_markup = []
        for cat in _categories:
            _cat_markup.append([cat.name])
        _cat_markup.append([constants.GO_BACK[0]])
        usr.state = 'm1'
        usr.save()
        bot.sendMessage(chat_id, 'Выберите категорию', reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup,
                        resize_keyboard = True))
    if text == constants.USER_MENU[2]:
        if text != constants.GO_BACK[0]:
            usr.state = 'a2'
            usr.state_text = '[]'
            state_json = ast.literal_eval(usr.state_text)
            _user = {
                'tid': from_id,
                'name': '',
                'text': '',
                'answer': '',
                'file': '',
                'category': '',
            }
            state_json.append(_user)
            usr.state_text = json.dumps(state_json)
            usr.save()
            _categories = Category.objects.all()
            _cat_markup = []
            for cat in _categories:
                _cat_markup.append([cat.name])
            _cat_markup.append([constants.GO_BACK[0]])
            bot.sendMessage(chat_id, 'Выберите категорию задания',
                            reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup, resize_keyboard=True))
        elif text == constants.GO_BACK[0]:
            usr.state = '0'
            usr.state_text = '[]'
            usr.save()
            bot.sendMessage(chat_id, 'Выберите раздел', reply_markup=user_markup)
    elif usr.state == 'a2':
        if text != constants.GO_BACK[0]:
            state_json = ast.literal_eval(usr.state_text)
            for state in state_json:
                state['category'] = text
            usr.state_text = json.dumps(state_json)
            usr.state = 'a3'
            usr.save()
            bot.sendMessage(chat_id, 'Введите название задания', reply_markup=back_markup)
        elif text == constants.GO_BACK[0]:
            usr.state = '0'
            usr.state_text = '[]'
            usr.save()
            bot.sendMessage(chat_id, 'Выберите раздел', reply_markup=user_markup)
    elif usr.state == 'a3':
        if text != constants.GO_BACK[0]:
            state_json = ast.literal_eval(usr.state_text)
            for state in state_json:
                state['name'] = text
            usr.state_text = json.dumps(state_json)
            usr.state = 'a4'
            usr.save()
            bot.sendMessage(chat_id, 'Введите текст задания', reply_markup=back_markup)
        elif text == constants.GO_BACK[0]:
            usr.state = 'a2'
            usr.save()
            _categories = Category.objects.all()
            _cat_markup = []
            for cat in _categories:
                _cat_markup.append([cat.name])
            _cat_markup.append([constants.GO_BACK[0]])
            bot.sendMessage(chat_id, 'Выберите категорию задания',
                            reply_markup=ReplyKeyboardMarkup(keyboard=_cat_markup, resize_keyboard=True))
    elif usr.state == 'a4':
        if text != constants.GO_BACK[0]:
            state_json = ast.literal_eval(usr.state_text)
            for state in state_json:
                state['text'] = text
            usr.state_text = json.dumps(state_json)
            usr.state = 'a5'
            usr.save()
            bot.sendMessage(chat_id, 'Введите ответ задания', reply_markup=back_markup)
        elif text == constants.GO_BACK[0]:
            usr.state = 'a3'
            usr.save()
            bot.sendMessage(chat_id, 'Введите название задания', reply_markup=back_markup)
    elif usr.state == 'a5':
        if text != constants.GO_BACK[0]:
            state_json = ast.literal_eval(usr.state_text)
            for state in state_json:
                state['answer'] = text
            usr.state_text = json.dumps(state_json)
            usr.state = 'a6'
            usr.save()
            bot.sendMessage(chat_id, 'Отправьте файл задания', reply_markup=back_markup)
        elif text == constants.GO_BACK[0]:
            usr.state = 'a4'
            usr.save()
            bot.sendMessage(chat_id, 'Введите текст задания', reply_markup=back_markup)




