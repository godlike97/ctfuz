import ast
import base64
import datetime
from web.models import UserTemp, Category, UserInfo, Task


def create_user(usr):
    state_json = ast.literal_eval(usr.state_text)
    _userInfo = UserInfo()
    for state in state_json:
        _userInfo.username = state['username']
        _userInfo.address = state['city']
        _userInfo.email = state['email']
        _userInfo.phone = state['phone']
        _userInfo.t_id = state['tid']
        _userInfo.register_date = datetime.datetime.now()
        _userInfo.solved_tasks = '0'
        _userInfo.is_active = True
        _userInfo.save()
        return True


def contact_validate(s):
    ans = False
    try:
        split_text1 = s.split(';')
        split_text2 = s.split(',')
        if len(split_text1) > len(split_text2):
            split_text = split_text1
        else:
            split_text = split_text2
        for i in range(len(split_text)):
            if len(split_text[i]) == 12:
                if (split_text[i][0] == '9') and (split_text[i][1] == '9') and (split_text[i][2] == '8') and (
                        split_text[i][3] == '9'):
                    if (split_text[i][4] == '0') or (split_text[i][4] == '1') or (split_text[i][4] == '3') or (
                            split_text[i][4] == '4') or (split_text[i][4] == '5') or (split_text[i][4] == '7') or (
                            split_text[i][4] == '8') or (split_text[i][4] == '9'):
                        ans = True
            elif len(split_text[i]) == 9:
                if split_text[i][0] == '9':
                    if (split_text[i][1] == '0') or (split_text[i][1] == '1') or (split_text[i][1] == '3') or (
                            split_text[i][1] == '1') or (split_text[i][1] == '5') or (split_text[i][1] == '7') or (
                            split_text[i][1] == '8') or (split_text[i][1] == '9'):
                        ans = True
    except:
        ans = False

    return ans


def create_task(usr):
    state_json = ast.literal_eval(usr.state_text)
    _task = Task()
    for state in state_json:
        _task.name = state['name']
        _task.answer = state['answer']
        _task.text = state['text']
        try:
            _task.category = Category.objects.get(name=state['category'])
        except:
            _task.category = Category.objects.first()
        _task.file = state['file']
        _task.from_tid = state['tid']
        _task.save()
        return True