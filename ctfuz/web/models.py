from django.db import models

# Create your models here.


class UserTemp(models.Model):
    t_id = models.CharField(max_length=100, null= True, blank=True)
    state = models.CharField(max_length=100, default='0')
    state_text = models.CharField(max_length=5000, default='[]', blank=True)


class UserInfo(models.Model):
    t_id = models.CharField(max_length=100, null=True, blank=True, verbose_name='Telegram ID',
                            help_text='Идентификатор Телеграма')
    username = models.CharField(max_length=100, verbose_name='Имя')
    phone = models.CharField(max_length=100, null=True, blank=True, verbose_name='Номер телефона')
    city = models.CharField(max_length=200, null=True, blank=True, verbose_name='Самарканд')
    is_blocked = models.BooleanField(default=False, verbose_name='Заблокирован?')
    email = models.CharField(max_length=150, default=False, verbose_name='Почта')
    register_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата регистрации')
    is_active = models.BooleanField(default=False, editable=False)
    solved_tasks = models.TextField(default='0', blank=False)
    total_points = models.IntegerField(default=0, blank=False, null=False)


class Category(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)


class Task(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    name = models.TextField(blank=False, null=False)
    text = models.TextField(blank=False, null=False)
    answer = models.TextField(blank=False, null=False)
    file = models.TextField(blank=True, null=True)
    first = models.ForeignKey(UserInfo, null=True, on_delete=models.PROTECT, related_name='First')
    second = models.ForeignKey(UserInfo, null=True, on_delete=models.PROTECT, related_name='Second')
    third = models.ForeignKey(UserInfo, null=True, on_delete=models.PROTECT, related_name='Third')
    from_tid = models.IntegerField(null=True, blank=True)
    point = models.IntegerField(null=True, blank=True)
